Vue.component('to-do', {
	props: ['todo'],
	template: '<div class="row"><div class="col-3">{{ todo.text }}</div> <div class="col-3"> price: <input type="number" v-model.number="todo.price"></div> <div class="col-3"> quantity: <input type="number" v-model.number="todo.quantity"></div></div>'
})

var app = new Vue({
	el: '#app',
	data: {
		clock: ''
	}
})

var timer = setInterval(updateClock, 1000);
updateClock();
function updateClock(){
	var time = new Date();
	app.clock = zeroPadding(time.getHours()) + ':' + zeroPadding(time.getMinutes()) + ':' + zeroPadding(time.getSeconds());
}

function zeroPadding(time){
	if(time<10){
		time = '0'+time;
	}
	return time

}

var app2 = new Vue({
	el: '#date',
	data: {
		message: 'You loaded this page on ' + new Date().toLocaleString(),
		styleObject: {
			color: 'red'
		}
	}
})

var app3 = new Vue({
	el: '#seen',
	data: {
		seen: true,
		status: "Hide"
	},
	methods: {
		messageVisibility: function(){
			if (this.seen == true){
				this.seen = false
				this.status = "Show"
			} else {
				this.seen = true
				this.status = "Hide"
			}
		}
	}
})

var app4 = new Vue({
	el: '#todo',
	data: {
		todos: [
		{ text: 'eat'},
		{ text: 'sleep'},
		{ text: 'pray'},
		]
	}
})

var app5 = new Vue({
	el: '#reverse',
	data: {
		message: 'Hello World!',
		count: 0
	},
	methods: {
		reverseMessage: function(){
			this.message = this.message.split('').reverse().join('')
			this.count += 1
		}
	}
})

var app6 = new Vue({
	el: '#input',
	data: {
		message: 'Hello Vue!'
	}
})

var app7 = new Vue({
	el: '#birthday',
	data: {
		age: null,
		year: null
	},
	methods: {
		calculateYear: function(){
			this.age = 'You are ' + (new Date().getFullYear() - this.year) + ' years old!'
		}
	}

})

var app8 = new Vue({
	el: '#component',
	data: {
		grocery: [
		{ id:0, text: 'Vegetables', price: 3, quantity: 0 },
		{ id:1, text: 'Cheese', price: 4, quantity: 0 },
		{ id:2, text: 'Pasta', price: 5, quantity: 0}
		],
		shopItem: null,
		message: "Press enter to submit"
	},
	methods: {
		addItem: function(){
			this.grocery.push({text: this.shopItem, price: 0, quantity: 0})
			this.shopItem = null
		}
	},
	computed: {
		totalPrice (){
			return this.grocery.reduce((total,item) => {
				return total + item.quantity*item.price
			}, 0)
		}
	}
})